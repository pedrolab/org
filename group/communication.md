# Types of digital communication in organizations

In the past, the differences between *oral* and *written* communication were more clear. With the digital era everything is written communication. The function of *oral communication* is still there: here it is defined as ephemeral communication. *Written communication* is defined as persistent communication. They have complementary properties:

| Property                  | Ephemeral                 | Persistent                        |
|---------------------------|---------------------------|-----------------------------------|
| Validity time             | From minutes to days      | Forever (or until further notice) |
| Completeness              | Less                      | More                              |
| Information reliability   | Less                      | More                              |
| Accessibility             | More                      | Less                              |
| Response time             | Less                      | More                              |
| Communication quality     | Less                      | More                              |
| Concurrency usage limit   | 1 per channel             | IT system limits                  |
| Information structure     | Less                      | More                              |

There are services that tend to work more in ephemeral or persistent way. Hence, providing just one platform for everything in your organization infrastructure is going to lack some communication properties.

Solution: select (at least) two services, one specialized in ephemeral and another in persistent communication. Users should choose when to use one system or the other. Careful when designing bridges (automatic messages traversing) between ephemeral and persistent systems, you are unifying the system again and loosing communication properties. When users traverse info between ephemeral and persistent systems it is fine because they adapt it to appropriate context and style.

Systems such as email are too flexible, hence, you need great procedure detail about how it is used in your organization. Alternatively, integrate email with another systems. I recommend moving 95% of ephemeral emails to chat and integrate persistent email with different persistent systems as needed.

- Chat (text, audio, video) use ephemeral communication properties.
  - A transformation from ephemeral to persistent occurs when taking minutes of meetings. Ideally, all participants should verify the information contained.
- Forums, mailing lists, ticketing systems and document repositories use persistent communication properties.
- Social networking services such as twitter, facebook and derivatives look like persistent but they are ephemeral.
