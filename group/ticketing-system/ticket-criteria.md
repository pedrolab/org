atomically, all tasks should be a ticket, but if the ticket is going to be empty, then it could be a task inside the most related ticket

## when a task could be inside a ticket

small task that fit into one line should reside on the same ticket, example:

- [ ] task1
  - additional content
  - a note
    - a side note
- [ ] task1
  - [ ] task1.1
  - [ ] task1.2

### task1.2 annex

a screen capture

a code block

## when that task deserves a ticket

- it's inside is going to have content and details, and that the simple task formatting is not enough
- you want to track progress over time, because it's complex, or because you want discussion around it
- you want to refer it in an autonomous number, because it has its self identity
