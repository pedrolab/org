*Ticket meeting* is a follow-up meeting that:

- it is a ticket itself
  - it is opened before the meeting to note topics to discuss
  - it is closed after the meeting when all the information is processed and everybody agrees that the ticket meeting represents what happened in the meeting
- metadata:
  - duration of the meeting
  - participants
- data or content: basically, bulletpoints:
  - referencing tickets: during the meeting some tickets are created, updated and closed. The target tickets have a 1:1 relation with the meeting:
    - a ticket meeting links to an issue ticket with a very brief description (around 1 line max)
    - an issue ticket links to a ticket meeting and appends all the information from the meeting and related to that issue. Detailed and specific as you need.
  - small notes, expressed in italic, when they are not enough to be expressed on a ticket
- ticket meeting are optionally linked in sequence (previous/next meeting) forming a ticket meeting chain

side notes: I started using it for the first time on 2019-10-12 on gitlab.com
